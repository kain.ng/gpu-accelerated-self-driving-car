#pragma once
#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>  //syscall

enum GPIO_PINS {
    GPIO_PIN7 = 216,
    GPIO_PIN11 = 50,
    GPIO_PIN12 = 79,
    GPIO_PIN13 = 14,
    GPIO_PIN15 = 194,
    GPIO_PIN16 = 232,
    GPIO_PIN18 = 15,
    GPIO_PIN19 = 16,
    GPIO_PIN21 = 17,
    GPIO_PIN22 = 13,
    GPIO_PIN40 = 78
};

enum GPIO_PIN_VALUE {
    HIGH, LOW
};

int gpio_export(int pin);
int gpio_set_direction(int pin, char * direction);
int gpio_get_pin_val(int pin);
int gpio_set_pin_val(int pin, int val);
int gpio_unexport(int pin);