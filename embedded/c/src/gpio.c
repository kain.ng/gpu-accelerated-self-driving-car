#include "../gpio.h"

int gpio_export(int pin) {
    int fd, slen;
    char s_pin[5];
    char path[] = "/sys/class/gpio/export";

    sprintf(s_pin, "%d", pin);
    slen = strlen(s_pin);

    fd = open(path, O_WRONLY); //O_WRONLY = open for write only
    if (fd == -1) {
       perror("Hi. Cannot open /sys/class/gpio/export");
       exit(1);
    }
    usleep(50000);
    if (write(fd, s_pin, slen) !=slen) {
       perror("Hi. Cannot write to /sys/class/gpio/export");
    }
    close(fd);
    return 0;
}

int gpio_set_direction(int pin, char * direction){
    int fd, slen, buf_size;
    char s_pin[5];
    char path[50] = "/sys/class/gpio/gpio";
    char dir[] = "/direction";

    sprintf(s_pin, "%d", pin);
    slen = strlen(s_pin);

    strcat(path, s_pin);    // /sys/class/gpio/gpioN
    strcat(path, dir);      // /sys/class/gpio/gpioN/direction

    fd = open(path, O_WRONLY);
    if (fd == -1) {
        perror("Cannot open /sys/class/gpio/gpio216/direction");
        exit(1);
    }
    
    if (strcmp(direction, "out") == 0) {
        buf_size = 3;
    } else if (strcmp(direction, "in") == 0) {
        buf_size = 2;
    } else {
        perror("Invalid direction");
    }
    if (write(fd, direction, buf_size) != buf_size) {
        perror("Cannot write to /sys/class/gpio/gpio216/direction");
    }

    close(fd);
    return 0;
}
 
int gpio_set_pin_val(int pin, int val) {
    char ch_val[2];
    int fd;
    char s_pin[5];
    char path[50] = "/sys/class/gpio/gpio";
    char value[] = "/value";

    sprintf(ch_val, "%d", val); //convert to char
    sprintf(s_pin, "%d", pin);

    strcat(path, s_pin);    // /sys/class/gpio/gpioN
    strcat(path, value);      // /sys/class/gpio/gpioN/value

    fd = open(path, O_WRONLY);
    if (fd == -1) {
        perror("Cannot open /sys/class/gpio/gpioN/value");
        exit(1);
    }
    
    if (write(fd, ch_val, 1) != 1) {
        perror("Cannot write to /sys/class/gpio/gpioN/value");
    }

    close(fd);
    return 0;
}

int gpio_get_pin_val(int pin) {
    char ch_val[2];
    int fd;
    char s_pin[5];
    char path[50] = "/sys/class/gpio/gpio";
    char value[] = "/value";

    sprintf(s_pin, "%d", pin);

    strcat(path, s_pin);    // /sys/class/gpio/gpioN
    strcat(path, value);      // /sys/class/gpio/gpioN/value

    fd = open(path, O_RDONLY);
    if (fd == -1) {
        perror("Cannot open /sys/class/gpio/gpioN/value");
        exit(1);
    }
    
    if (read(fd, ch_val, 1) != 1) {
        perror("Cannot write to /sys/class/gpio/gpioN/value");
    }

    close(fd);
    return atoi(ch_val);
}

int gpio_unexport(int pin) {
    int fd, slen;
    char s_pin[5];
    char path[] = "/sys/class/gpio/unexport";

    sprintf(s_pin, "%d", pin);
    slen = strlen(s_pin);

    fd = open(path, O_WRONLY); //O_WRONLY = open for write only
    if (fd == -1) {
       perror("Cannot open /sys/class/gpio/unexport");
       exit(1);
    }
    if (write(fd, s_pin, slen) !=slen) {
       perror("Cannot write to /sys/class/gpio/unexport");
    }
    close(fd);
    return 0;
}


