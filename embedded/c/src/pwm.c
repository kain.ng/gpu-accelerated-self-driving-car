#include "../pwm.h"

int pwm_export(int pin){
    int fd;
    char s_pin[5];
    char pwmchip[50] = "/sys/class/pwm/pwmchip";
    char export[] = "/export";

    sprintf(s_pin, "%d", pin);
    strcat(pwmchip, s_pin);
    strcat(pwmchip, export);

    fd = open(pwmchip, O_WRONLY);
    if (fd == -1) {
        perror("Cannot open pwmchipN/export");
        exit(1);
    }
    if (write(fd, s_pin, 1) != 1) {
        perror("Cannot write to pwmchipN/export");
    }
    
    close(fd);   
    return 0; 
}

int pwm_enable(int pin){
    int fd;
    char s_pin[5];
    char pwmchip[100] = "/sys/class/pwm/pwmchip";
    char pwm[] = "/pwm";
    char enable[] = "/enable";

    sprintf(s_pin, "%d", pin); 
    strcat(pwmchip, s_pin);     //  /sys/class/pwm/pwmchip0
    strcat(pwmchip, pwm);       //  /sys/class/pwm/pwmchip0/pwm
    strcat(pwmchip, s_pin);     //  /sys/class/pwm/pwmchip0/pwm0
    strcat(pwmchip, enable);    //  /sys/class/pwm/pwmchip0/pwm0/enable

    // enable the pin by writing 1
    fd = open(pwmchip, O_WRONLY);
    if (fd == -1) {
        perror("Cannot open pwmchipN/pwmN/enable");
        exit(1);
    }
    if (write(fd, "1", 1) != 1) {
        perror("Cannot write to pwmchip0/pwm0/enable");
    }
    close(fd);
    return 0;
}

int pwm_disable(int pin){
    int fd;
    char s_pin[5];
    char pwmchip[100] = "/sys/class/pwm/pwmchip";
    char pwm[] = "/pwm";
    char enable[] = "/enable";

    sprintf(s_pin, "%d", pin); 
    strcat(pwmchip, s_pin);     //  /sys/class/pwm/pwmchip0
    strcat(pwmchip, pwm);       //  /sys/class/pwm/pwmchip0/pwm
    strcat(pwmchip, s_pin);     //  /sys/class/pwm/pwmchip0/pwm0
    strcat(pwmchip, enable);    //  /sys/class/pwm/pwmchip0/pwm0/enable

    // enable the pin by writing 1
    fd = open(pwmchip, O_WRONLY);
    if (fd == -1) {
        perror("Cannot open pwmchipN/pwmN/enable");
        exit(1);
    }
    if (write(fd, "0", 1) != 1) {
        perror("Cannot write to pwmchip0/pwm0/enable");
    }
    close(fd);
    return 0;
}

int pwm_unexport(int pin){
    int fd;
    char s_pin[5];
    char pwmchip[50] = "/sys/class/pwm/pwmchip";
    char unexport[] = "/unexport";

    sprintf(s_pin, "%d", pin);
    strcat(pwmchip, s_pin);
    strcat(pwmchip, unexport);

    fd = open(pwmchip, O_WRONLY);
    if (fd == -1) {
        perror("Cannot open pwmchipN/uexport");
        exit(1);
    }
    if (write(fd, s_pin, 1) != 1) {
        perror("Cannot write to pwmchipN/uexport");
    }
    
    close(fd);   
    return 0; 
}

int pwm_set_period(int pin, int time_us) {
    int fd;
    int slen;
    char s_pin[5];
    char s_period[10];
    char pwmchip[100] = "/sys/class/pwm/pwmchip";
    char pwm[] = "/pwm";
    char period[] = "/period";

    sprintf(s_pin, "%d", pin); 
    sprintf(s_period, "%d", time_us);
    slen = strlen(s_period);
    
    strcat(pwmchip, s_pin);     //  /sys/class/pwm/pwmchip0
    strcat(pwmchip, pwm);       //  /sys/class/pwm/pwmchip0/pwm
    strcat(pwmchip, s_pin);     //  /sys/class/pwm/pwmchip0/pwm0
    strcat(pwmchip, period);    //  /sys/class/pwm/pwmchip0/pwm0/period

    fd = open(pwmchip, O_WRONLY);
    if (fd == -1 ) {
        perror("Cannot open pwmchip0/pwm0/period");
        exit(1);
    }
    if (write(fd, s_period, slen) != slen){  //set 1 period to 1000000us = 1sec
        perror("Cannot write to pwmchip0/pwm0/period");
    }
    close(fd);
    return 0;
}

// duty cycle time_us = yourInput/period time_us
int pwm_set_dutyCycle(int pin, int time_us) {
    int fd;
    int slen;
    char s_pin[5];
    char s_time[10];
    char pwmchip[100] = "/sys/class/pwm/pwmchip";
    char pwm[] = "/pwm";
    char dutyCycle[] = "/duty_cycle";

    sprintf(s_pin, "%d", pin); 
    sprintf(s_time, "%d", time_us);
    slen = strlen(s_time);

    strcat(pwmchip, s_pin);     //  /sys/class/pwm/pwmchip0
    strcat(pwmchip, pwm);       //  /sys/class/pwm/pwmchip0/pwm
    strcat(pwmchip, s_pin);     //  /sys/class/pwm/pwmchip0/pwm0
    strcat(pwmchip, dutyCycle);    //  /sys/class/pwm/pwmchip0/pwm0/period

    fd = open(pwmchip, O_WRONLY);
    if (fd == -1 ) {
        perror("Cannot open pwmchip0/pwm0/duty_cycle");
        exit(1);
    }
    if (write(fd, s_time, slen) != slen){  //set 1 period to 1000000us = 1sec
        perror("Cannot write to pwmchip0/pwm0/duty_cycle");
    }
    close(fd);
    return 0;
}




