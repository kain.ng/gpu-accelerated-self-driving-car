#pragma once
#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>  //syscall

enum PWM_PINS{
    PWM_PIN32 = 0,
    PWM_PIN33 = 4
};

int pwm_export(int pin);
int pwm_enable(int pin);
int pwm_disable(int pin);
int pwm_unexport(int pin);
int pwm_set_period(int pin, int time_us);
int pwm_set_dutyCycle(int pin, int time_us);