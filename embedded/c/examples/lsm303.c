#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <linux/i2c.h>
#include <linux/i2c-dev.h>

#include <stdio.h>
#include <stdlib.h>

#define LEN 2

void read_register(int fd, struct i2c_rdwr_ioctl_data data, __u16 addr) {
    int retval;

    data.msgs[0].addr = 0x19;
    data.msgs[0].flags = 0;
    data.msgs[0].len = 1;
    data.msgs[0].buf = malloc(1);
    data.msgs[0].buf[0] = addr;

    data.msgs[1].addr = 0x19;
    data.msgs[1].flags = I2C_M_RD;
    data.msgs[1].len = 1;
    data.msgs[1].buf = malloc(1);
    data.msgs[1].buf[0] = 0;

    retval = ioctl(fd, I2C_RDWR, &data);
    if (retval < data.nmsgs)
        printf("Error: some messages were not sent\n");

    //printf("sent %d messages\n", retval);

    printf("%02x ", data.msgs[1].buf[0]);

    free(data.msgs[0].buf);
    free(data.msgs[1].buf);
}

int main() {
    struct i2c_rdwr_ioctl_data data;
    int i, fd, retval;

    fd = open("/dev/i2c-0", O_RDWR);
    if (fd < 0) {
        printf("Error: open() returned %d\n", fd);
        return 0;
    }

    //retval = ioctl(fd, I2C_SLAVE, 0x19);
    //printf("return code from setting slave addr: %d\n", retval)l


    data.msgs = malloc(LEN * sizeof(*data.msgs));
    data.nmsgs = LEN;

    for (int i = 0; i < 1000; i++) {
        read_register(fd, data, 0x27);
        printf(" x:");
        read_register(fd, data, 0x29);
        read_register(fd, data, 0x28);
        printf(" y:");
        read_register(fd, data, 0x2B);
        read_register(fd, data, 0x2A);
        
        printf(" z:");
        read_register(fd, data, 0x2D);
        read_register(fd, data, 0x2C);
        
        puts("");
        usleep(100000);
    }

    close(fd);

    return 0;
}