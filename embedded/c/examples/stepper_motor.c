/*
    This is an example of using DRV8825 to control the rotation of a NEMA 17 stepper motor
    GPIO Pin 7 & Pin 15 are used to control the STEP & DIR pin respectively
 */

 #include "../gpio.h"

const int DIR_PIN = GPIO_PIN7;
const int STEP_PIN = GPIO_PIN15;

const int CLOCKWISE = 1;
const int COUNTERCLOCKWISE = 0;
const int SPR = 48;  //steps per revolution  = 360/7.5 degree angle according to data sheet

int main(void) {
    unsigned int delay = 50000; // 0.05s

    gpio_export(DIR_PIN);
    gpio_export(STEP_PIN);

    gpio_set_direction(DIR_PIN, "out");
    gpio_set_direction(STEP_PIN, "out");

    gpio_set_pin_val(DIR_PIN, CLOCKWISE);

    for (int i = 0; i < SPR*50; ) {
        gpio_set_pin_val(STEP_PIN, 1);
        usleep(delay);
        gpio_set_pin_val(STEP_PIN, 0);
        usleep(delay);        
    }


    gpio_unexport(DIR_PIN);
    gpio_unexport(STEP_PIN);
    return 0;
 }