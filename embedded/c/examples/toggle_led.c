/*
    This is an example that toggles an led using Pin7
*/

#include "../gpio.h"

#define NUMBER_OF_TOGGLES 50

int main(void) {

    gpio_export(GPIO_PIN11);
    gpio_set_direction(GPIO_PIN11, "out");
    gpio_set_pin_val(GPIO_PIN11, HIGH);

    //set pin 21 to 0 & 1 to toggle output voltage
    for (int i = 0; i < NUMBER_OF_TOGGLES; i++) {
        gpio_set_pin_val(GPIO_PIN11, HIGH);
        usleep(1000000);
        printf("to low\n");
        
        gpio_set_pin_val(GPIO_PIN11, LOW);
        usleep(1000000);
    }

    gpio_unexport(GPIO_PIN11);
    return 0;
 
}