/*
    This is an example of using DM542T to control the rotation of a NEMA 17 stepper motor
    PWM Pin 32 & Pin 33 are used to control the DIR & PUL pin respectively

    period from 300,000 (fastest) to 20,000,000 (slowest) 1khz
 */

#include "../pwm.h"
#include "../gpio.h"

const int DIR_PIN = GPIO_PIN21;
const int PUL_PIN = PWM_PIN32;

const int CLOCKWISE = 1;
const int COUNTERCLOCKWISE = 0;
const int SPR = 200;  //pulse per revolution = 200

void set_half_duty_cycle(int period) {
    pwm_set_dutyCycle(PUL_PIN, period/2);
}


int main(void) {
    int period = 0;
    unsigned int delay = 50; // 0.05s

    pwm_export(PUL_PIN);
    pwm_enable(PUL_PIN);
    pwm_set_period(PUL_PIN, 1000000);
    pwm_set_dutyCycle(PUL_PIN, 90000);

    gpio_export(DIR_PIN);
    gpio_set_direction(DIR_PIN, "out");
    gpio_set_pin_val(DIR_PIN, COUNTERCLOCKWISE);
    usleep(delay);
    printf("done pwm setup \n");
    

        for(int i = 1; i < 10000; i++) {
            period = 20000000;
            gpio_set_pin_val(DIR_PIN, COUNTERCLOCKWISE);
            pwm_set_period(PUL_PIN, period);
            set_half_duty_cycle(period);
            usleep(delay);
            printf("counterclockwise, period = %d\n", period);
        }
        //usleep(delay*20);
        /*for(int i = 1; i < 10000; i++) {
            period = 20000000;
            gpio_set_pin_val(DIR_PIN, CLOCKWISE);
            pwm_set_period(PUL_PIN, period);
            set_half_duty_cycle(period);
            usleep(delay);
            printf("Clockwise, period = %d\n", period);
        }
        //usleep(delay*20);
        for(int i = 1; i < 10000; i++) {
            period = 400000;
            gpio_set_pin_val(DIR_PIN, COUNTERCLOCKWISE);
            pwm_set_period(PUL_PIN, period);
            set_half_duty_cycle(period);
            usleep(delay);
            printf("counterclockwise, period = %d\n", period);
        }
        //usleep(delay*20);
        for(int i = 1; i < 10000; i++) {
            period = 300000;
            gpio_set_pin_val(DIR_PIN, CLOCKWISE);
            pwm_set_period(PUL_PIN, period);
            set_half_duty_cycle(period);
            usleep(delay);
            printf("Clockwise, period = %d\n", period);
        }*/        
    

    pwm_disable(PUL_PIN);
    pwm_unexport(PUL_PIN);

    gpio_unexport(DIR_PIN);
    return 0;
 }