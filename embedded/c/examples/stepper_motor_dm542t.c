/*
    This is an example of using DM542T to control the rotation of a NEMA 17 stepper motor
    GPIO Pin 21 & Pin 22 are used to control the DIR & PUL pin respectively
 */

 #include "../gpio.h"

const int DIR_PIN = GPIO_PIN21;
const int PUL_PIN = GPIO_PIN22;

const int CLOCKWISE = 1;
const int COUNTERCLOCKWISE = 0;
const int SPR = 200;  //pulse per revolution = 200


int main(void) {
    //gpio_unexport(DIR_PIN);
    //gpio_unexport(PUL_PIN);
    unsigned int delay = 50; // 0.05s

    gpio_export(DIR_PIN);
    gpio_export(PUL_PIN);

    gpio_set_direction(DIR_PIN, "out");
    gpio_set_direction(PUL_PIN, "out");

    gpio_set_pin_val(DIR_PIN, COUNTERCLOCKWISE);
    usleep(delay);
    gpio_set_pin_val(PUL_PIN, 0);
    printf("done cc setup 1ms\n");
    
    //while(1){

        for(int i = 1; i < 5000; i++) {
            gpio_set_pin_val(PUL_PIN, 0);  //1 ccw
            usleep(delay*1);
            gpio_set_pin_val(PUL_PIN, 1);  //0 ccw
            usleep(delay*9);
            printf(",");
        }

    printf("\n");
        gpio_set_pin_val(DIR_PIN, CLOCKWISE);
        for(int i = 1; i < 5000; i++) {
            gpio_set_pin_val(PUL_PIN, 0);  //1 ccw
            usleep(delay*1);
            gpio_set_pin_val(PUL_PIN, 1);  //0 ccw
            usleep(delay*9);
            printf("-");
        }
        
    //}
    printf("\n");
    //gpio_unexport(DIR_PIN);
    //gpio_unexport(PUL_PIN);
    return 0;
 }