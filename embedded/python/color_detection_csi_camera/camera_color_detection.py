import cv2
import numpy as np
import pandas as pd
import argparse

# Reading csv file with pandas and giving names to each column
index = ["color", "color_name", "hex", "R", "G", "B"]
csv = pd.read_csv('colors.csv', names=index, header=None)

# function to calculate minimum distance from all colors and get the most matching color
def getColorName(R, G, B):
    minimum = 10000
    for i in range(len(csv)):
        d = abs(R - int(csv.loc[i, "R"])) + abs(G - int(csv.loc[i, "G"])) + abs(B - int(csv.loc[i, "B"]))
        if (d <= minimum):
            minimum = d
            cname = csv.loc[i, "color_name"]
    return cname



r = g = b = 0
x = 480  #display_width/2
y = 270  #display_height/2
x1 = x-5
y1 = y-5
x2 = x+5
y2 = y+5

#set up camera gstreamer
def gstreamer_pipeline(
    sensor_id=0,
    capture_width=1920,  #720p 60fps
    capture_height=1080, #30fps
    display_width=960,
    display_height=540,
    framerate=30,
    flip_method=0,
):
    return (
        "nvarguscamerasrc wbmode=5 sensor-id=%d !"
        "video/x-raw(memory:NVMM), width=(int)%d, height=(int)%d, framerate=(fraction)%d/1 ! "
        "nvvidconv flip-method=%d ! "
        "video/x-raw, width=(int)%d, height=(int)%d, format=(string)BGRx ! "
        "videoconvert ! "
        "video/x-raw, format=(string)BGR ! appsink"
        % (
            sensor_id,
            capture_width,
            capture_height,
            framerate,
            flip_method,
            display_width,
            display_height,
        )
    )

#set up camera gstreamer
def gstreamer_pipeline_wb5(
    sensor_id=0,
    capture_width=1920,  #720p 60fps
    capture_height=1080, #30fps
    display_width=960,
    display_height=540,
    framerate=30,
    flip_method=0,
):
    return (
        "nvarguscamerasrc wbmode=5 sensor-id=%d !"
        "video/x-raw(memory:NVMM), width=(int)%d, height=(int)%d, framerate=(fraction)%d/1 ! "
        "nvvidconv flip-method=%d ! "
        "video/x-raw, width=(int)%d, height=(int)%d, format=(string)BGRx ! "
        "videoconvert ! "
        "video/x-raw, format=(string)BGR ! appsink"
        % (
            sensor_id,
            capture_width,
            capture_height,
            framerate,
            flip_method,
            display_width,
            display_height,
        )
    )    


def show_camera():
    window_title = "CSI Camera"

    # To flip the image, modify the flip_method parameter (0 and 2 are the most common)
    print(gstreamer_pipeline())
    video_capture = cv2.VideoCapture(gstreamer_pipeline(flip_method=0), cv2.CAP_GSTREAMER)
    if video_capture.isOpened():
        try:
            window_handle = cv2.namedWindow(window_title, cv2.WINDOW_AUTOSIZE)
            while True:
                ret_vl, frame = video_capture.read()
                
                cv2.rectangle(frame, (x1, y1), (x2, y2), (0,0,255), 2)
                b, g, r = frame[y, x]
                text = getColorName(r, g, b)
                print('r, g, b = ', r, ', ',g,', ', b, "       \tcolor = ", text)
            
                
                # Check to see if the user closed the window
                # Under GTK+ (Jetson Default), WND_PROP_VISIBLE does not work correctly. Under Qt it does
                # GTK - Substitute WND_PROP_AUTOSIZE to detect if window has been closed by user
                if cv2.getWindowProperty(window_title, cv2.WND_PROP_AUTOSIZE) >= 0:
                    cv2.imshow(window_title, frame)
                else:
                    break 
                keyCode = cv2.waitKey(100) & 0xFF
                # Stop the program on the ESC key or 'q'
                if keyCode == 27 or keyCode == ord('q'):
                    break
        finally:
            video_capture.release()
            cv2.destroyAllWindows()
    else:
        print("Error: Unable to open camera")



def test_wb():
    window_title = "CSI Camera"
    while True:
        print("-------------------- GStreamer = first wb")
        video_capture = cv2.VideoCapture(gstreamer_pipeline_auto(flip_method=0), cv2.CAP_GSTREAMER)
        window_handle = cv2.namedWindow(window_title, cv2.WINDOW_AUTOSIZE)
        for i in range(1, 100):
            ret_vl, frame = video_capture.read()
            cv2.rectangle(frame, (x1, y1), (x2, y2), (0,0,255), 2)
            b, g, r = frame[y, x]
            text = getColorName(r, g, b)
            print('r, g, b = ', r, ', ',g,', ', b, "       color = ", text)
            if cv2.getWindowProperty(window_title, cv2.WND_PROP_AUTOSIZE) >= 0:
                cv2.imshow(window_title, frame)
            else:
                break 
            keyCode = cv2.waitKey(100) & 0xFF
            # Stop the program on the ESC key or 'q'
            if keyCode == 27 or keyCode == ord('q'):
                break
        video_capture.release()
        cv2.destroyAllWindows()    

        



if __name__ == "__main__":
    show_camera()
    #test_wb()


    