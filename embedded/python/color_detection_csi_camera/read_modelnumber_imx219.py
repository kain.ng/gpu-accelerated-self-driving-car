from smbus2 import SMBus, i2c_msg
import time

address = 0x10


bus = SMBus(6)

print('bus #\n')

time.sleep(10)

bus.write_byte_data(address, 0x00, 0x00)
result = bus.read_byte(address)

print("result = ", result)