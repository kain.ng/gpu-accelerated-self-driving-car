import RPi.GPIO as GPIO
import time 

#GPIO.setwarnings(False)

led_pin = 12 #output
but_pin = 40 #input


def main():
    count = 0
    #pin setup
    
    GPIO.setmode(GPIO.BOARD)
    GPIO.setup(led_pin, GPIO.OUT)
    GPIO.setup(but_pin, GPIO.IN)

    GPIO.output(led_pin, GPIO.LOW) #initially output set to low



    '''
    print("start testing now")
    try:
        while True:
            
            curr_value = GPIO.input(but_pin)
            if curr_value == 0:
                print("curr_value == 0")
                GPIO.output(led_pin, GPIO.HIGH)
                time.sleep(2)
                GPIO.output(led_pin, GPIO.LOW)
            if curr_value == 1:
                print("curr_value == 1")
     
    finally:
        GPIO.cleanup()
    '''


    try:
        while True:
            print("Waiting for button event")
            GPIO.wait_for_edge(but_pin, GPIO.FALLING)

            # event received when button pressed
            print("Button Pressed!")
            GPIO.output(led_pin, GPIO.HIGH)
            time.sleep(1)
            GPIO.output(led_pin, GPIO.LOW)
    finally:
        GPIO.cleanup()  # cleanup all GPIOs





if __name__ == '__main__':
    main()